package ungs.pp2.main;

import ungs.pp2.model.WeatherService;
import ungs.pp2.model.connectors.WeatherConnector;
import ungs.pp2.model.dto.Weather;

public class Main {


    public static void main(String[] args) {
        WeatherService service = new WeatherService(new WeatherConnector());
        Weather response = service.getWeather(4, "pilar-buenos-aires-argentina");
        System.out.println(response.toString());
    }

}