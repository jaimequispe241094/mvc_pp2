package ungs.pp2.model;

import com.google.common.collect.Maps;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class Configuration {

    private Properties properties;
    private Map<String, String> mapValues = Maps.newHashMap();

    public Configuration(String path) {
        this.properties = new Properties();

        try {
            properties.load((new FileReader(path)));
            mapValues.putAll(Maps.fromProperties(properties));
        }
        catch (FileNotFoundException e) {
            // "El archivo de Configuracion %s no existe. "
            throw new RuntimeException("El archivo no existe");
        }
        catch (IOException e) {
            // "Error al leer el archivo de configuracion: "
            throw new RuntimeException("Error al leer el archivo");
        }
    }

    public Map<String, String> getValues() {
        return mapValues;
    }

}