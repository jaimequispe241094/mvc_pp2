package ungs.pp2.model;

public interface RestConnector<T> {

    T getData(Integer days, String locationId);

}