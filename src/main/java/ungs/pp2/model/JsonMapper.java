package ungs.pp2.model;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.Optional;

public class JsonMapper {

    private static JsonMapper JSONPARSER;
    private Gson gson;

    private JsonMapper() {
        this.gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
    }

    public static JsonMapper getMapper() {
        JSONPARSER = Optional.ofNullable(JSONPARSER).orElse(new JsonMapper());
        return JSONPARSER;
    }

    public <T> T getValue(String json, Type type) {
        return gson.fromJson (json, type);
    }

    public String toJson(Object object) {
        return gson.toJson(object);
    }

    public String toJson(String pathFile) {
        try {
            return new JsonParser().parse(new FileReader(pathFile)).getAsString();
        } catch (FileNotFoundException fnfe) {
            throw new RuntimeException("Archivo no encontrado");
        }
    }

}