package ungs.pp2.model;

import ungs.pp2.model.dto.Weather;

public class WeatherService {

    private RestConnector<Weather> connector;

    public WeatherService(RestConnector<Weather> connector) {
        this.connector = connector;
    }

    public Weather getWeather(Integer days, String locationId) {
        return connector.getData(days, locationId);
    }

}