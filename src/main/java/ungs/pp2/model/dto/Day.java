package ungs.pp2.model.dto;

public class Day {

    private Double mintempC;
    private Double maxtempC;
    private Double maxTempF;
    private Double minTempF;

    public Double getMintempC() {
        return mintempC;
    }

    public void setMintempC(Double mintempC) {
        this.mintempC = mintempC;
    }

    public Double getMaxtempC() {
        return maxtempC;
    }

    public void setMaxtempC(Double maxtempC) {
        this.maxtempC = maxtempC;
    }

    public Double getMaxTempF() {
        return maxTempF;
    }

    public void setMaxTempF(Double maxTempF) {
        this.maxTempF = maxTempF;
    }

    public Double getMinTempF() {
        return minTempF;
    }

    public void setMinTempF(Double minTempF) {
        this.minTempF = minTempF;
    }

    @Override
    public String toString() {
        return "Day{" +
                "mintempC=" + mintempC +
                ", maxtempC=" + maxtempC +
                ", maxTempF=" + maxTempF +
                ", minTempF=" + minTempF +
                '}';
    }

}