package ungs.pp2.model.dto;

public class Current {

    private Double tempC;
    private Double tempF;
    private Double feelslikeC;
    private Double feelslikeF;

    public Double getTempC() {
        return tempC;
    }

    public void setTempC(Double tempC) {
        this.tempC = tempC;
    }

    public Double getTempF() {
        return tempF;
    }

    public void setTempF(Double tempF) {
        this.tempF = tempF;
    }

    public Double getFeelslikeC() {
        return feelslikeC;
    }

    public void setFeelslikeC(Double feelslikeC) {
        this.feelslikeC = feelslikeC;
    }

    public Double getFeelslikeF() {
        return feelslikeF;
    }

    public void setFeelslikeF(Double feelslikeF) {
        this.feelslikeF = feelslikeF;
    }

    @Override
    public String toString() {
        return "Current{" +
                "tempC=" + tempC +
                ", tempF=" + tempF +
                ", feelslikeC=" + feelslikeC +
                ", feelslikeF=" + feelslikeF +
                '}';
    }

}