package ungs.pp2.model.connectors;

import org.apache.http.client.fluent.Request;
import ungs.pp2.model.Configuration;
import ungs.pp2.model.JsonMapper;
import ungs.pp2.model.RestConnector;
import ungs.pp2.model.dto.Weather;

public class WeatherConnector implements RestConnector<Weather> {

    private JsonMapper mapper = JsonMapper.getMapper();
    private Configuration conf = new Configuration("conf/configuration.properties");
    private String url = conf.getValues().get("weather.url");

    @Override
    public Weather getData(Integer days, String locationId) {
        String response = null;
        try {
            response = Request.Get(buildUrl(days, locationId)).execute().returnContent().asString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapper.getValue(response, Weather.class);
    }

    private String buildUrl(Integer days , String locationId) {
        return this.url.replace("$DAYS", days.toString())
                       .replace("$LOCATION", locationId);
    }

}