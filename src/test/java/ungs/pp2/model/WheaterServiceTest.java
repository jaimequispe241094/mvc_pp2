package ungs.pp2.model;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ungs.pp2.model.connectors.WeatherMockConnector;
import ungs.pp2.model.dto.Weather;

public class WheaterServiceTest {

    private static WeatherService instance;

    @BeforeClass
    public static void setUp() {
        instance = new WeatherService(new WeatherMockConnector());
    }

    @Test
    public void testEmpty() {
        Weather w = this.instance.getWeather(2, "error");
        Assert.assertNull(w);
    }

}